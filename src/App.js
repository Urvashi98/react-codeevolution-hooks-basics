import './App.css';
// import UseEffectExample from './components/useEffectExample';
// import HookRunOnce from './components/HookEffectOnlyOnce';
// import ToggleHookEffectOnlyOnce from './components/ToggleHookEffectOnlyOnce';
// import IntervalHookCounter from './components/IntervalHookCounter';
import DataFetching from './components/DataFetching';
import HookTimer from './components/HookTimer';
import ReduxExample from './components/ReduxExample';
import Parent from './components/useCallbackDemo/Parent';
import UseMemoExample from './components/UseMemoExample';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        {/* <UseEffectExample /> */}
        {/* <HookRunOnce /> */}
        {/* <ToggleHookEffectOnlyOnce /> */}
        {/* <IntervalHookCounter /> */}
        {/* <DataFetching /> */}
        {/* <UseMemoExample /> */}
        {/* <HookTimer /> */}
        {/* <Parent /> */}
        <ReduxExample />
      </header>

    </div>
  );
}

export default App;
