import React from "react";

function Count(props) {
    const { text, count } = props;
    console.log(`Count - ${text}`, count)
  return (
    <div>
      <span>
        {text} - {count}
      </span>
    </div>
  );
}
// export default Count;

export default React.memo(Count);
