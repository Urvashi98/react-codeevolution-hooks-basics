import React, { useCallback } from "react";
import Title from "./Title";
import { useState } from "react";
import Count from "./Count";
import Button from "./Button";

function Parent() {
  const [age, setAge] = useState(22);
  const [salary, setSalary] = useState(15000);

    const incrementAge = useCallback(() => {
        setAge(age + 1);
    }, [age]);
    const inrcrementSalary = useCallback(() => {
        setSalary(salary + 1);
    },[salary]);
    
//   const incrementAge = () => {
//     // console.log("in increment age");
//     setAge(age + 1);
//   };
//   const inrcrementSalary = () => {
//     setSalary(salary + 500);
//   };
  return (
    <div>
      <h2>Use CallBak Demo Component</h2>
      <Title />
      <Count text="Age" count={age} />
      <Button handleClick={incrementAge}>Age ++</Button>
      <Count text="Salary" count={salary} />
      <Button handleClick={inrcrementSalary}>Salary ++</Button>
    </div>
  );
}

export default Parent;
