import React, { useMemo } from "react";
import { useState } from "react";

function UseMemoExample() {
  const [counterOne, setcounterOne] = useState(0);
  const [counterTwo, setcounterTwo] = useState(0);

  function handleClickCounterOne() {
    setcounterOne(counterOne + 1);
  }

  function handleClickCounterTwo() {
    setcounterTwo(counterTwo + 1);
  }
 //check if no is even or odd //useMemo can return values, useRef cant
const isEven = useMemo(()=> {
        let i = 0;
        while (i < 10000) {
            i++;
            console.log('in whirte')
        }
    return counterOne % 2 === 0;
  },[counterOne])
    
 

    //rendering isEven() on evevry btn click, makes the entire component tree slower.
    //hence, we can place extensively calculative funcs in useMemo() hook.
    //useMemo() will persist/cache the result and will display from that while other components/controls are being operated!
    
    //counter One was also slowing down counterTwo. hence use useMemo()
    
    
  return (
    <div>
      <div className="container">
        <div className="row">
          <div className="col-md-6">
            <button onClick={handleClickCounterOne}>
              Counter One :{counterOne}
            </button>
            <span>{isEven ? "Even" : "Odd"}</span>
          </div>
          <div className="col-md-6">
            <button onClick={handleClickCounterTwo}>
              Counter Two :{counterTwo}
            </button>
          </div>
        </div>
      </div>
    </div>
  );
}

export default UseMemoExample;
