import React, { useEffect } from "react";
import { useState } from "react";

const HookRunOnce = () => {
  const [XY, setXY] = useState({
    X: 0,
    Y: 0,
  });
  const catchMovement = (e) => {
    console.log("in catch");
    setXY((prev) => ({ ...prev, X: e.clientX, Y: e.clientY }));
  };
  const unMountfunc = () => {
    console.log("In UnMount COde");
    window.removeEventListener("mousemove", catchMovement);
  };

  //no dependency to call everytime, hence call once using [] as 2nd arg!
  useEffect(() => {
    console.log("in effect");
    
    window.addEventListener("mousemove", catchMovement);

    return unMountfunc;
  },[]);


  return (
    <div>
      X: {XY.X} and Y: {XY.Y}
    </div>
  );
};

export default HookRunOnce;
