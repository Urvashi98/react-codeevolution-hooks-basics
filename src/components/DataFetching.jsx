import axios from "axios";
import React, { useEffect } from "react";
import { useState } from "react";

function DataFetching() {
  const [posts, setPosts] = useState([]);
  const [id, setId] = useState(0);

  function handleChange(event) {
    setId(event.target.value);
  }

  function getPostBytId(id) {
    console.log(id);
    axios
      .get(`https://jsonplaceholder.typicode.com/posts/${id}`)
      .then((res) => {
        setPosts(res.data);
      })
      .catch((err) => console.log(err));
  }

  function getallPosts() {
    axios
      .get("https://jsonplaceholder.typicode.com/posts")
      .then((res) => {
        setPosts(res.data);
      })
      .catch((err) => console.log(err));
  }

  useEffect(() => {
    //all posts
    getallPosts();

    //specific post as per id
      getPostBytId(id);
      
  }, [id]); //pass id depen if search by id.

  return (
    <div>
      <input type="text" value={id} onChange={handleChange} />
      <br />
      {posts.title}
      <ul>
        {posts.length > 0
          ? posts.map((post) => <li key={post.id}>{post.title}</li>)
          : null}
      </ul>
    </div>
  );
}

export default DataFetching;
