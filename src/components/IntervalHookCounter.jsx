import React, { useEffect } from "react";
import { useState } from "react";

const IntervalHookCounter = () => {
  const [count, setCount] = useState(0);

  const tick = () => {
    // setCount((count) => (count + 1)); //setCOubt takes care of tge previous values
    //hence, no need to pass it to the dependency array in Use Effect

    //opt 2
    setCount(count + 1); //here the useEfect will have to keep track of
    //the updation in count values
  };

  //opt 1
  /* useEffect(() => {
      const interval =  setInterval(tick, 1000)
        
        return () => {
            clearInterval(interval)
        }
    },[]); */

  //opt 2
/*   useEffect(() => {
    const interval = setInterval(tick, 1000);

    return () => {
      clearInterval(interval);
    };
  }, [count]); */

  return <>{count}</>;
};

export default IntervalHookCounter;
