import React, { useState, useEffect } from "react";
import store from "./../redux/store";
import { BUG_ADDED, BUG_REMOVED, BUG_UPDATED } from "../redux/actions";

const initialState = {
  id: 0,
  description: "",
  resolved: false,
};

function ReduxExample() {
  const [data, setData] = useState(initialState);

  /*   console.log("store in redux comp", store.getState()); */
  /*  console.log("store in redux comp", store.getState());

  store.dispatch({
    type: BUG_ADDED,
    payload: {
      id: 1,
      description: "My First Bug added!",
      resolved: true,
    },
  });
  store.dispatch({
    type: BUG_ADDED,
    payload: {
      id: 2,
      description: "My second Bug added!",
      resolved: false,
    },
  });
  store.dispatch({
    type: BUG_ADDED,
    payload: {
      id: 3,
      description: "My third Bug added!",
      resolved: true,
    },
  });

  console.log("after Bug adding", store.getState());

  store.dispatch({
    type: BUG_REMOVED,
    payload: {
      id: 3,
    },
  });

  console.log("after Bug removing", store.getState()); */
  const handleinput = (event) => {
    let type = event.currentTarget.type;
    let key = event.target.name;
    let value = event.target.value;

    if (type === "checkbox") {
      setData({ ...data, resolved: event.target.checked });
    } else {
      setData({ ...data, [key]: value });
    }
  };
  const handleSubmit = (event) => {
    event.preventDefault();
    const id = Math.floor(Math.random() * 100 + 1);
    console.log("ID", id);
    // console.log("DATA", data);
    const getData = { ...data, id };
    console.log("getData:", getData);
    setData(getData);

    //add to state
    /*   store.dispatch({
      type: BUG_ADDED,
      payload: data,
    }); */

    console.log("after Bug adding", store.getState());
  };

  useEffect(() => {
    store.dispatch({
      type: BUG_ADDED,
      payload: data,
    });
  }, [data]);
  console.log("data", data);
  return (
    <>
      <h3>Redux Example</h3>
      <form onSubmit={handleSubmit}>
        <input type="text" name="description" onChange={handleinput} />
        <br />
        Resolved:{" "}
        <input type="checkbox" name="resolved" onChange={handleinput} />
        <br />
        <button>Add New Bug</button>
      </form>
    </>
  );
}

export default ReduxExample;
