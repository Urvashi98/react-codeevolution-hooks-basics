import { useEffect, useState } from "react";

const UseEffectExample = () => {
  const [count, setCount] = useState(0);
  const [name, setName] = useState("");

  const handleClick = () => {
    setCount(count + 1);
  };
  //updates dom every time!
  /* useEffect(() => {
    document.title = `You Clicked ${count} times`;
  }); */

  //conditionaly render useEffect hook, here, when the count changes
  useEffect(() => {
    document.title = `You Clicked ${count} times`;
    console.log("in use effect !");
  }, [count]);

  return (
    <>
      <h4>You Clicked ${count} times</h4>
      <input
        type="text"
        value={name}
        onChange={(e) => setName(e.target.value)}
      />
      <button onClick={handleClick}>CLick </button>
    </>
  );
};

export default UseEffectExample;
