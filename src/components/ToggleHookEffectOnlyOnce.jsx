import React from 'react';
import HookRunOnce from './HookEffectOnlyOnce';
import { useState } from 'react';

const ToggleHookEffectOnlyOnce = () => {

    const [display, setDisplay] = useState(true);
    return <>
        {display ? <HookRunOnce /> : null}
        <button onClick={() => setDisplay(!display)}>Toggle Display</button>
    </>;
};

export default ToggleHookEffectOnlyOnce;
