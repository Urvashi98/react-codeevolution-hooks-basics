import { createStore } from 'redux';
import reducer from './reducer';



const store = createStore(reducer); //Higher order Function, it takes function as argument :)
export default store;


//store has methods like dispatch, subscribe, getstate etc that will be helpful 
//in UI rendering and manipulation.

//to change the state we have only one entry point. using actionS nd PAYLOADS.


//DISPATCH the action
//SUBSCRIBE the store - notify the chnage in state
//GETSTATE - get current state