import { BUG_ADDED, BUG_REMOVED, BUG_UPDATED } from "./actions";

let lastId = 0;

let initialState = [
  {
    id: 1,
    description: "My First Bug added!",
    resolved: true,
  },
  {
    id: 2,
    description: "My second Bug added!",
    resolved: false,
  },
  {
    id: 3,
    description: "My third Bug added!",
    resolved: true,
  },
];

export default function reducer(state = initialState, action) {
  //switch case
  switch (action.type) {
    case BUG_ADDED:
      return [
        ...state,
        {
          id: action.payload.id,
          description: action.payload.description,
          resolved: action.payload.resolved,
        },
      ];
    case BUG_REMOVED:
      return state.filter((bug) => bug.id !== action.payload.id);
    default:
      return state;
  }

  //using if else
  /*    if (action.type === BUG_ADDED) {
        return [...state, {
            id: ++lastId,
            description: action.payload.description,
            resolved: false
        }];
    }
    else if (action.type === BUG_REMOVED) {
        return state.filter(bug => bug.id !== action.payload.id)
    }
    else {
        return state;
    } */
}
